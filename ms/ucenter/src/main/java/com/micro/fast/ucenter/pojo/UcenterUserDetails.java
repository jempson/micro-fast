package com.micro.fast.ucenter.pojo;

import java.io.Serializable;
import java.util.Date;

public class UcenterUserDetails implements Serializable {
    private Integer id;

    private String signature;

    private String realName;

    private Date birthday;

    private String question;

    private String answer;

    private static final long serialVersionUID = 1L;

    public UcenterUserDetails(Integer id, String signature, String realName, Date birthday, String question, String answer) {
        this.id = id;
        this.signature = signature;
        this.realName = realName;
        this.birthday = birthday;
        this.question = question;
        this.answer = answer;
    }

    public UcenterUserDetails() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature == null ? null : signature.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question == null ? null : question.trim();
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer == null ? null : answer.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", signature=").append(signature);
        sb.append(", realName=").append(realName);
        sb.append(", birthday=").append(birthday);
        sb.append(", question=").append(question);
        sb.append(", answer=").append(answer);
        sb.append("]");
        return sb.toString();
    }
}