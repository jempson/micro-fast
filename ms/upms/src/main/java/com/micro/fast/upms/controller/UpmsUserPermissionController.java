package com.micro.fast.upms.controller;

import com.micro.fast.boot.starter.common.response.ServerResponse;
import com.micro.fast.upms.service.UpmsUserPermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
*
* @author lsy
*/
@Api("upmsUserPermission")
@RestController
@RequestMapping("/upmsUserPermission")
public class UpmsUserPermissionController {

  @Autowired
  private UpmsUserPermissionService upmsUserPermissionService;

  @ApiOperation("批量添加用户和组织的关系,一个用户可以属于多个组织")
  @PostMapping("/batch/oneUserWithManyOrganization")
  public ServerResponse batchAddUpmsUserPermission(@RequestParam("userId") Integer userId,@RequestParam("orgIds") ArrayList<Integer> orgIds) {

    return null;
  }
}
