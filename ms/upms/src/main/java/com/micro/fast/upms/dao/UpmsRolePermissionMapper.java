package com.micro.fast.upms.dao;

import com.micro.fast.common.dao.SsmMapper;
import com.micro.fast.upms.pojo.UpmsRolePermission;

import java.util.List;

public interface UpmsRolePermissionMapper extends SsmMapper<UpmsRolePermission,Integer> {
    int deleteByPrimaryKey(Integer rolePermissionId);

    int insert(UpmsRolePermission record);

    int insertSelective(UpmsRolePermission record);

    UpmsRolePermission selectByPrimaryKey(Integer rolePermissionId);

    int updateByPrimaryKeySelective(UpmsRolePermission record);

    int updateByPrimaryKey(UpmsRolePermission record);

}